package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class Home {
    @Autowired
    private HomeRepo homeRepo;

    public Home(HomeRepo homeRepo) {
        this.homeRepo = homeRepo;
    }

    @GetMapping("/")
    public List<AnimalModel> home(){

        return homeRepo.findAll();
    }
}
